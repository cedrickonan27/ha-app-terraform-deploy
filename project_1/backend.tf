terraform {
  backend "s3" {
    bucket         = "my-regular-bucket"
    key            = "ha-app-project_1/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    # access_key     = "$AWS_ACCESS_KEY_ID"
    # secret_key     = "$AWS_SECRET_ACCESS_KEY"
  }
}